const { app, BrowserWindow, dialog, Menu } = require('electron');
// app - Module to control application life.
// BrowserWindow - Module to create native browser window.
const fs = require('fs');
const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
const windows = new Set();
const fileWatchers = new Map();

const createWindow = exports.createWindow = (file) => {
  // Create the browser window.
    let newWindow = new BrowserWindow({
        width: 800,
        height: 600,
        show: false
    });

    windows.add(newWindow);
  // and load the index.html of the app.
    newWindow.loadURL(`file://${__dirname}/index.html`);

    newWindow.once('ready-to-show', () => {
        // if (file) openFile(newWindow, file);
        newWindow.show();
    });
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

    // Emitted before the window is closed.
    newWindow.on('close', function () {
      if(newWindow.isDocumentEdited()) {
        event.preventDefault();
        const result = dialog.showMessageBox(newWindow, {
            type: 'warning',
            title: 'Quit with unsaved changes?',
            message: 'Unsaved changes will be lost.',
            buttons: [
                'Quit Anyway',
                'Cancel'
            ],
            defaultId: 0,
            cancelId: 1
        });
        if(result === 0) newWindow.destroy();
      }
    });
  // Emitted when the window is closed.
    newWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
        windows.delete(newWindow);
        stopWatchingFile(newWindow);
        newWindow = null;
    })
};

const startWatchingFile = (win, file) => {
  stopWatchingFile(win);
  const watcher = fs.watch(file, (event) => {
    if(event === 'change') {
        const content = fs.readFileSync(file).toString();
        win.webContents.send('file-changed', file, content);
    }
  });
  fileWatchers.set(win, watcher);
};
const stopWatchingFile = (win) => {
  if(fileWatchers.has(win)) {
    fileWatchers.get(win).close();
    fileWatchers.delete(win);
  }
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {

    const template = [
        {
            label: "File",
            submenu: [{
                label: "Save",
                click(item, focusedWindow) {
                    if(focusedWindow) focusedWindow.webContents.send('save-file');
                },
                accelerator: 'CommandOrControl+X'
            }]
        }
    ];
    if( process.platform === 'darwin') {
        template.unshift({
            label: "No one will see me"
        })
    }
    const application = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(application);
    createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('will-finish-launching', () => {
  app.on('open-file', (event, filePath) => {
    createWindow(filePath);
  })
});

// app.on('activate', function () {
//   // On OS X it's common to re-create a window in the app when the
//   // dock icon is clicked and there are no other windows open.
//   if (newWindow === null) {
//     createWindow()
//   }
// });

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

const getFileFromUserSelection = exports.getFileFromUserSelection = (targetWindow) => {
  const files = dialog.showOpenDialog(targetWindow, {
      properties: ['openFile'],
      filters: [
          { name: 'Text Files', extensions: ['txt', 'text'] },
          { name: 'Markdown Files', extensions: ['md', 'markdown'] },
      ]
  });
  if (!files) return;
  return files[0];
};

const openFile = exports.openFile = (targetWindow, filePath) => {
    const file = filePath || getFileFromUserSelection(targetWindow);
    const content = fs.readFileSync(file).toString();
    startWatchingFile(targetWindow, file);
    targetWindow.webContents.send('file-opened', file, content);
    targetWindow.setTitle(`${file} - Fire Sale`);
    targetWindow.setRepresentedFilename(file);
};

const saveMarkdown = exports.saveMarkdown = (win, file, content) => {
  if(!file) {
    file = dialog.showSaveDialog(win, {
        title: 'Save Markdown',
        // defaultPath: app.getPath('documents'),
        filters: [
            { name: 'Markdown Files', extensions: ['md', 'markdown'] }
        ]
    })
  }
  if (!file) return;

  fs.writeFileSync(file, content);
  win.webContents.send('file-opened', file, content);
};